#ifndef SRC_S21_MATRIX_H_
#define SRC_S21_MATRIX_H_
#include <iostream>
#include <math.h>

class S21Matrix {
 private:
        int _rows, _cols;
        double **_matrix;

 public:
        S21Matrix(): _rows(1), _cols(1) {
            _matrix = new double *[_rows];
            _matrix[0] = new double[_cols]{};
        }

        S21Matrix(int rw, int cl): _rows(rw), _cols(cl) {
            _matrix = new double *[_rows];
            for (int i = 0; i < _rows; i++) {
                _matrix[i] = new double[_cols]{};
            }
        }

        S21Matrix(const S21Matrix& other): _rows(other._rows), _cols(other._cols) {
            _matrix = new double *[other._rows];
            for (int i = 0; i < other._rows; i++) {
                _matrix[i] = new double[other._cols];

                for (int j = 0; j < other._cols; j++) {
                    _matrix[i][j] = other._matrix[i][j];
                }
            }
        }

        S21Matrix(S21Matrix&& other): _rows(other._rows), _cols(other._cols), _matrix(other._matrix) {
            if (other._matrix == NULL) {
                throw std:: invalid_argument("Incorrect matrix");
            }
            other._matrix = nullptr;
            other._rows = 0;
            other._cols = 0;
        }

        double& operator() (int row, int col) {
            if ( row < 0 || col < 0 || row > _rows  || col > _cols ) {
                throw std:: invalid_argument("Index incorrect");
            }
            return _matrix[row][col];
        }

        S21Matrix operator= (S21Matrix other) {
            if (this != &other) {
                this->~S21Matrix();
                this->create_matrix(other._rows, other._cols);
                for (int i = 0; i < _rows; i++) {
                    for (int j = 0; j < _cols; j++) {
                        _matrix[i][j] = other._matrix[i][j];
                    }
                }
            }
            return *this;
        }

        S21Matrix operator+ (S21Matrix other) {
            S21Matrix tmp(*this);
            tmp.sum_matrix(other);
            return tmp;
        }

        S21Matrix operator- (S21Matrix other) {
            S21Matrix tmp(*this);
            tmp.sub_matrix(other);
            return tmp;
        }

        bool operator== (S21Matrix other) {
            return this->eq_matrix(other);
        }

        S21Matrix operator+= (S21Matrix other) {
            S21Matrix tmp = *this;
            *this = tmp + other;
            tmp.~S21Matrix();
            return *this;
        }

        S21Matrix operator-= (S21Matrix other) {
            S21Matrix tmp = *this;
            *this = tmp - other;
            tmp.~S21Matrix();
            return *this;
        }

        S21Matrix operator* (S21Matrix other) {
            S21Matrix tmp(*this);
            tmp.mul_matrix(other);
            return tmp;
        }

        S21Matrix operator* (double num) {
            S21Matrix tmp(*this);
            tmp.mul_number(num);
            return tmp;
        }

        S21Matrix operator*= (S21Matrix other) {
            this->mul_matrix(other);
            return *this;
        }

        S21Matrix operator*= (double num) {
            this->mul_number(num);
            return *this;
        }

        ~S21Matrix() {
            for (int i = 0; i < _rows; i++) {
                delete [] this->_matrix[i];
                _matrix[i] = NULL;
            }
            delete [] this->_matrix;
            _matrix = NULL;
            _rows = 0;
            _cols = 0;
        }

        bool eq_matrix(const S21Matrix& other);
        void sum_matrix(const S21Matrix& other);
        void sub_matrix(const S21Matrix& other);
        void mul_number(const double num);
        void mul_matrix(const S21Matrix& other);
        S21Matrix transpose();
        S21Matrix calc_complements();
        double determinant();
        S21Matrix inverse_matrix();
        int getRows() const;
        int getCols() const;
        void create_matrix(int i, int j);
        S21Matrix create_minor_matrix(const int X, const int Y);
        void free_memory();

};
#endif  //  SRC_S21_MATRIX_H_
