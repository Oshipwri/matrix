#include "s21_matrix.h"

bool S21Matrix::eq_matrix(const S21Matrix& other) {
    bool result = true;
    if (_rows == other._rows && _cols == other._cols) {
        for (int i = 0; i < _rows && result == true; i++) {
            for (int j = 0; j < _cols && result == true; j++) {
                if (fabs(_matrix[i][j] - other._matrix[i][j]) > 1e-7) {
                    result = false;
                }
            }
        }
    } else {
        result = false;
    }

    return result;
}

void S21Matrix::sum_matrix(const S21Matrix& other) {
    if (_rows != other._rows || _cols != other._cols) {
        throw std::invalid_argument("Different size of matrixs");
    }

    for (int i = 0; i < _rows; i++) {
        for (int j = 0; j < _cols; j++) {
            _matrix[i][j] += other._matrix[i][j];
        }
    }
}

void S21Matrix::sub_matrix(const S21Matrix& other) {
    if (_rows != other._rows || _cols != other._cols) {
        throw std::invalid_argument("Different size of matrixs");
    }
    
    for (int i = 0; i < _rows; i++) {
        for (int j = 0; j < _cols; j++) {
            _matrix[i][j] -= other._matrix[i][j];
        }
    }
}

void S21Matrix::mul_number(const double num) {
    for (int i = 0; i < _rows; i++) {
        for (int j = 0; j < _cols; j++) {
            _matrix[i][j] *= num;
        }
    }
}

void S21Matrix::mul_matrix(const S21Matrix& other) {
    if (_cols != other._rows) {
        throw std::invalid_argument("Matrix multiplication is not possible");
    }
    int rw = this->_rows;
    int cl = this->_cols;
    S21Matrix tmp = *this;
    this->free_memory();
    this->create_matrix(rw, other._cols);
    for (int i = 0; i < _rows; i++) {
        for (int j = 0; j < _cols; j++) {
            for (int k = 0; k < cl; k++) {
                _matrix[i][j] += tmp._matrix[i][k] * other._matrix[k][j];
            }
        }
    }
}

S21Matrix S21Matrix::transpose() {
    S21Matrix tmp(_cols, _rows);
    for (int i = 0; i < tmp._rows; i++) {
        for (int j = 0; j < tmp._cols; j++) {
            tmp._matrix[i][j] = _matrix[j][i];
        }
    }
    return tmp;
}

double S21Matrix::determinant() {
    double result = 0.0;

    if (_rows != _cols) {
         throw std::invalid_argument("The matrix is not square");
    }
    if (_cols == 1) {
        result = this->_matrix[0][0];
    } else if (_cols == 2) {
        result = this->_matrix[0][0] * this->_matrix[1][1] -
        this->_matrix[1][0] * this->_matrix[0][1];
    } else {
        for (int i = 0;  i < _cols; i++) {
            S21Matrix minor = this->create_minor_matrix(0, i);
            result += this->_matrix[0][i] * minor.determinant() * pow(-1, i);
        }
    }
    return result;
}

S21Matrix S21Matrix::create_minor_matrix(const int X, const int Y) {
    S21Matrix result(_rows - 1, _cols - 1);

    for (int i = 0, i_new = 0; i < _rows; i++) {
        if (i == X) continue;
        for (int j = 0, j_new = 0; j < _cols; j++) {
            if (j == Y) continue;
            result._matrix[i_new][j_new] = _matrix[i][j];
            j_new++;
        }
        i_new++;
    }
    return result;
}

S21Matrix S21Matrix::inverse_matrix() {
    if (determinant() == 0) {
        throw std::invalid_argument("The matrix is not square");
    }

    S21Matrix result_tmp_1 = calc_complements();
    S21Matrix result_tmp_2 = result_tmp_1.transpose();
    result_tmp_2.mul_number(1/determinant());
    return result_tmp_2;
}

S21Matrix S21Matrix::calc_complements() {
    S21Matrix result(_rows, _cols);

    if (_rows != _cols) {
        throw std::invalid_argument("The matrix is not square");
    }

    for (int i = 0; i < _rows; i++) {
        for (int j = 0; j < _cols; j++) {
            S21Matrix minor = this->create_minor_matrix(i, j);
            result._matrix[i][j] = minor.determinant() * pow(-1, i + j);
        }
    }
    return result;
}

void S21Matrix::create_matrix(int i, int j) {
    _rows = i;
    _cols = j;
    _matrix = new double *[_rows]();
    for (int g = 0; g < _rows; g++) {
        _matrix[g] = new double[_cols]();
    }
}

void S21Matrix::free_memory() {
    this->~S21Matrix();
}